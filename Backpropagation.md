# Jaringan Syaraf Tiruan dan Backpropagation

## Penjelasan Jaringan Syaraf Tiruan (neural Network) secara Sederhana

Secara sederhana, sebuah jaringan saraf tiruan yang tersupervisi bisa 
digambarkan sebagai blackbox dengan 2 moteode untuk pembelajaran dan prediksi 
sebagai berikut:
1. fase learning: Input => pembelajaran (input, output) Update kondisi internal <= Output
2. Fase prediksi: Input => Menggunakan kondisi internal => Output

Proses pembelajaran menggunakan input dan output yang dikehendaki dan mengupdate 
kondisi internal, sehingga output yang dihitung sedekat mungkin dengan hasil 
yang dikehendaki.
Fase prediksi menerima input dan menggunakan kondisi internal untuk menghasilkan
output sesuai dengan "pengalaman pembelajaran". Oleh karena itu machine learning 
kadang disebut dengan **model fitting**

## Contoh perhitungan sederhana



